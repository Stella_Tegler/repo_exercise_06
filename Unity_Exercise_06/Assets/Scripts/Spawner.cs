﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Spawner : MonoBehaviour
    {
        public int spawnCounter;

        private void Spawning()
        {
            for (int i = 0; i < spawnCounter; i++)
            {
                GameObject newGo = new GameObject();
                Debug.Log(message: "Oh man something spawned");
            }
        }

        private void Spawning2(int a)
        {
            while (a < spawnCounter)
            {
                GameObject newGo = new GameObject();
                spawnCounter++;
            }
        }

        private void Spawning3(int b)
        {
            do
            {
                GameObject newGo = new GameObject();
                spawnCounter++;
            }
            while (b < spawnCounter);
        }

        // Start is called before the first frame update
        void Start()
        {
            Spawning();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}