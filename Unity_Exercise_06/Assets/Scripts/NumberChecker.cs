﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class NumberChecker : MonoBehaviour
    {
        public int[] numbers;

        public int numberToSkip = 3;
        public int numberToBreakout = 5;

        private void Counter()
        {

            for (int i = 0; i < numbers.Length; i++)
            {
                Debug.Log(message: numbers[i]);

                if (i == numbers.Length)
                {
                    Debug.Log(message: "Loopy is finished, mission succesfull");
                }
            }
        }

        private void Counter2(int a)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == a)
                {
                    Debug.Log(message: "We skip boi");
                }

                Debug.Log(message: "Loopy 2 is finished, mission succesfull");
            }
        }

        private void Counter3(int b, int c)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == b)
                {
                    Debug.Log(message: "We skip again");
                }

                if (numbers[i] == c)
                {
                    Debug.Log(message: "We brake out bois");
                    break;
                }

                Debug.Log(message: "Loopy 3 is finished, mission succesfull");
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            Counter();
            Counter2(5);
            Counter3(2, 5);
        }
    }
}